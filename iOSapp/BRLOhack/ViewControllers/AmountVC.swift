//
//  AmountVC.swift
//  BRLOhack
//
//  Created by Goran Vuksic on 30/09/2017.
//  Copyright © 2017 Pirates. All rights reserved.
//

import UIKit
import Stripe

class AmountVC: UIViewController, STPPaymentCardTextFieldDelegate {

    let cardField = STPPaymentCardTextField()
    var theme = STPTheme.default()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        view.addSubview(cardField)
        edgesForExtendedLayout = []
        cardField.backgroundColor = theme.secondaryBackgroundColor
        cardField.textColor = theme.primaryForegroundColor
        cardField.placeholderColor = theme.secondaryForegroundColor
        cardField.borderColor = theme.accentColor
        cardField.borderWidth = 1.0
        cardField.textErrorColor = theme.errorColor
        cardField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonOrderTouchUpInside () {
        self.performSegue(withIdentifier: "step5", sender: nil)
    }
    
    func done() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let padding: CGFloat = 26
        cardField.frame = CGRect(x: padding,
                                 y: 300,
                                 width: view.bounds.width - (padding * 2),
                                 height: 50)
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.cvc?.characters.count == 3 {
            textField.resignFirstResponder()
        }
    }

}


