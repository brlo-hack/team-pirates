//
//  CustomVC.swift
//  BRLOhack
//
//  Created by Goran Vuksic on 30/09/2017.
//  Copyright © 2017 Pirates. All rights reserved.
//

import UIKit

class CustomVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var imageOption1: UIImageView!
    @IBOutlet weak var imageOption2: UIImageView!
    @IBOutlet weak var imageToEdit: UIImageView!
    
    @IBOutlet weak var textEditing: UITextField!
    @IBOutlet weak var textToShow: UITextField!

    var imageToShow: UIImage = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let tapOption1 = UITapGestureRecognizer(target: self, action: #selector(option1press))
        self.imageOption1.addGestureRecognizer(tapOption1)
        let tapOption2 = UITapGestureRecognizer(target: self, action: #selector(option2press))
        self.imageOption2.addGestureRecognizer(tapOption2)
        
        self.imageToEdit.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonFinishTouchUpInside () {
        self.performSegue(withIdentifier: "step4", sender: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.imageToEdit.image = self.imageToShow
    }
    
    @objc func option1press() {
        self.imageOption1.image = UIImage(named: "00_label_gray")
        self.imageOption2.image = UIImage(named: "01_label_black")
        self.imageToEdit.layer.cornerRadius = 0.0
    }

    @objc func option2press() {
        self.imageOption1.image = UIImage(named: "00_label_black")
        self.imageOption2.image = UIImage(named: "01_label_grey")
        self.imageToEdit.layer.cornerRadius = 80.0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        let resultString = text.replacingCharacters(in: range, with: string)
        
        self.textToShow.text = resultString
        return true
    }

}


